We would like to thank the reviewers for their thoughtful reviews.
All of the reviews were focused on the question of novelty relative to
work in dependent type theory, but before we answer that, we'd like to
note that the answers to reviewer 3's specific questions are at the
end of the review.

The short, unhelpful, answer to the question of novelty is that λert
refinement type system, and not a dependent type theory, and no one
has produced a refinement type system with explicit proofs before, nor
has anyone given such a system a denotational semantics -- this is by
definition novel.

A useful answer takes more longer, and requires explaining what the
difference between refinement types and dependent types actually is,
at a semantic level rather than a pragmatic one. This also lets us
explain:

1. The relationship between our logical variables and erasure, 
   and why modal approaches to erasure (such as QTT and Sterling-Harper)
   don't work for us. 
2. Why Awodey/Bauer squash types are also insufficient for our purpose
3. Why the simplicity of our semantics represents an advance 
   relative to systems like ICC. 

We do not have a specific change list, but would appreciate any
suggestions from the reviewers about what to focus on in our
revisions. 

### Intrinsic and Extrinsic Semantics 

To understand what is going on wth refinement types, it is worth
recalling that there are two main styles of doing denotational
semantics, which Reynolds named "intrinsic" and "extrinsics" in his
paper *The Meaning of Types: From Intrinsic to Extrinsic Semantics*.

The intrinsic style is the usual style of categorical semantics -- we
find some category where types and contexts are objects, and a terms
$\Gamma \vdash e : A$ is interpreted as an element of the hom-set
$\mathrm{Hom}(\Gamma, A)$. (Dependency does make things more
complicated technically, but not in any conceptually essential
fashion.) In the intrinsic style, only well-typed terms have
denotations, and ill-typed terms are grammatically ill-formed and do
not have a semantics at all. Another way of putting this is that 
intrinsic semantics interprets *typing derivations* rather than 
*terms*. 

In the extrinsic style, the interpretation function is defined upon
raw terms, and ill-typed programs *do* get a semantics. Types are then
defined as retracts of the underlying semantic object interpreting the
raw terms. A good example of this style of semantics is Milner's proof
of type safety for ML: he gave a denotational semantics for an
untyped lambda calculus, and then gave an interpretation of types by
logical relations over this untyped calculus, which let him use the
fundamental lemma to extract type safety. Logical relations (and
realizabilty methods in general) are typical examples of extrinsic
semantics.

Normally type theorists elide the distinction between these two styles
of semantics, because we prove a bracketing theorem that shows
coherence -- i.e., that the same term cannot have two derivations with
different semantics.

Intersection types puts these two styles of semantics together in a
different way. We start with a unrefined, base language with an
intrinsic semantics, and then define a *second* system of types as a
family of retracts over the base types. In λert, we use a monadic
lambda-calculus as the base language, and define our system of
refinements over this calculus, with the refinements structured
as a fibration over the base calculus. 

We deliberately avoided foregrounding the categorical machinery in our
paper to make it more accessible, though we do direct readers to
Mellies and Zeilberger's POPL 2015 paper (which deeply informed our
design). We could revisit this choice if the reviewers think it 
appropriate. 

Another, perhaps more familiar, instance of this framework are Hoare
logic and separation logic, which can be understood as refinement type
systems over a base "unityped" imperative language. In fact, the
distinction in Hoare logic between "logical variables" (which appear
in specifications) and program variables is (in semantic terms)
precisely the same as the distinction between logical and
computational terms in λert.

### Logical Variables and Erasure

The critical feature of Hoare-style logical variables is that they are
not program variables, and cannot influence the runtime behaviour of a
program – they only exist for specification purposes. So in a type

     vlen : ∀n:ℕ. Vec n → {x : ℕ | x = n}

where `∀n:N` is an intersection-style quantifier, it is only possible
to compute the length of the list by actually traversing the list. In
plain MLTT, the corresponding type has a degenerate implementation:

     vlen : Πn:ℕ. Vec n → Σx : ℕ. x = n
     vlen n _ = (n, refl) 

We should note that solving this problem was one of the motivations
behind the design of the ICC, and Matúš Tejiščák's PhD thesis
reiterates that this is different from proof irrelevance (since
different values of `n` are not equal).

Tejiščák's thesis contains two approaches for managing irrelevance.
One is a static program analysis, which has the usual tradeoffs of a
noncompositional static analysis versus our compositional semantic
approach. The other is a system of annotations inspired by Atkey and
McBride's QTT – this approach is indeed compositional, but (following
Curry-Howard) forces the identification of the refinement logic and
the type theory. 

This is undesirable for our purposes. We plan to use λert as the basis
of extending practical SMT-based refinement type systems with explicit
proofs, and SMT solvers are fundamentally based on classical logic.
(SAT for classical logic is NP-complete, but is PSPACE-complete for
intutionistic propositional logic!) So we want the semantics of the
propositions in our refinement types to be classical as well, which is
not possible when propositions and types are identified.
 
This is also the main obstacle to using the Sterling-Harper approach,
which uses a pair of modalities to control whether a term is in the
spec or runtime phases. Once again, re-using type-theoretic
connectives as logical connectives forces the identification of the
refinement logic and the type theory.

### Refinement logic and Squash Types

The need for design freedom to change the refinement logic is also one
of the two reasons why squash types are not suitable as the basis of
our type system. With the Awodey-Bauer squash type `[A]`, logical
disjunctions and existentials are encoded as

    P ∨ Q   = [P + Q]
    ∃x:X. P = [Σx:X. P]

As a result, if the type theory is intuitionistic the logic of
propositions must be as well. 

However, there is a deeper problem with using squash types. The
semantics of the Awodey-Bauer squash type is such that if `P` is a
proposition, then `P` and `[P]` are isomorphic.  Since `P` is a
proposition if all its inhabitants are equal, a contractible type like
`Σx:ℕ. x = n` is a proposition, and hence there is a map `[Σx:ℕ. x = n] →
Σx:ℕ. x = n`.

That is, it is possible to extract computational data from a squashed
type, and so erasure of propositions and squashed types is a much more
subtle problem than it may first seem. (Kraus *et al*'s paper *Notions
of Anonymous Existence in Martin-Löf Type Theory* studies this and
similar issues in detail.)

In contrast, λert erasure's is easy: since it is functorial, it is a
totally compositional and syntax-driven operation, and no propositions
can possibly exist at runtime.

### ICC and λert 

A number of the reviwers noted that our semantics seemed simple and
straightforward. We are glad that the reviewers thought so, since 
it indicates we achieved one of our design goals! However, achieving 
this goal is actually not as simple a problem as it may seem, and
a comparison with the ICC can illustrate the design issues. 

As we noted earlier, the design of the implicit calculus of
constructions was motivated by many of the same concerns we were, and
as we did it introduced an intersection type `∀x:T. U` to support
computationally irrelevant quantification. However, because there was
no separation of the refinement layer from the base layer, the
denotational semantics of the ICC become much more complicated – the
Luo-style ECC has a simple set-theoretic model, but Miquel (in his
paper *A Model for Impredicative Type Systems, Universes, and
Subtyping*) has to give a denotational semantics based on coherence
spaces. This is (in our view) a large jump in the complexity of the
model.

In contrast, we are able to model intersection types with ordinary
set-theoretic intersections. The reason for this is that the
refinement type discipline ensures that we only ever make use of
*structural* set-theoretic operations. That is, every λert type is a
subset of an underlying base type, and so when we take an
intersection, we are only taking the intersection of a family of
subsets of a particular set (the base type). From a mathematical point
of view, this is much better-behaved than taking intersections of
*arbitrary* sets, and having this invariant lets us interpret
intersections more simply than is possible in the semantics of the
ICC.

We did not claim this as a contribution because (in our view) the 
key idea is already present in Zeilberger and Mellies. We are, 
however, willing to expand our explanation of it. 

### Reviewer 3's Questions

1. > Section 3 heavily employs lists as a leading example but they are
   > not present in the language (as mentioned on lines 634-636). What
   > would be the challenges to extend the language with such inductive
   > types and refinements on these ?

   It's very easy to add support for inductive types (we only omitted it
to save space), and it follows the same pattern as for natural numbers.
The same is true for coinductive types. 

   If you wanted to support full mixed-variance types (e.g., `μa. (a →
a)`) then matters would be more complicated: the semantics of the
base language would need to move to a full domain-theoretic model 
capable of interpreting such types. 

2. > What's the impact in terms of expressivity of the absence of
   > extensionality rules for pairs, subsets, etc in the equality rules ?
   > Similarly, most congruences of equality can be obtained through the
   > Subst rule but I do not see a ξ-rule (congruence for λ-abstraction),
   > e.g `Γ, x : A ⊢ p : t = u ⇒ Γ ⊢ ξ(x ↦ p) : λ x : A. t = λ x : A. u`

   The η-rules for pairs and subsets are already derivable in λert,
using the Let-Pair-Pr and Let-Set-Pr rules. For example, the term
proving extensionality for subset types is:

        λ^||a : {x:A | ϕ(x)}||. 
          let {y,v} = a in
           β-let-set {y,v} ((z,w) ↦ (let {x,u} = {z,w} in C({x,u})) = C({z,w}))
   
        : ∀a : {x:A | ϕ(x)}. (let {x,u} = a in C({x,u}) = C(a)

   This uses the rules Gen, Let-Set-Pr, and β-Set. The congruence rule
for λ-abstraction is also validated by our model, and its omission is
an oversight, which we will correct.

   However, as our paper notes, function extensionality is not validated
by our model. We would need to move from a subset fibration/logical
predicates model to a richer PER semantics to support it.

3. > Even if the paper acknowleges explicitly the absence of higher order
   > features, reasoning on higher order functions can be extremely
   > useful. How does function equality work? Can anything be proved for
   > higher-order functions (map, filter, etc)?

   When we say "first-order logic", what we mean is that quantifiers
in the refinment logic can range over any λert type, but cannot 
range over prop itself. Eg, in higher-order logic, induction can
be stated as a formula: 

        ind : ∀P : ℕ → prop. P(0) ∧ (∀k:ℕ. P(k) → P(k+1)) → ∀n:ℕ. P(n)
                
   Here, the quantification over `P` is higher-order, and the
quantification over `k` and `n` is first-order. Without higher-order
quantification, induction is an axiom schema rather than an axiom.

   But since we regard all λert types as first-order sorts, we can still
make assertions about λert functions which quantify over
functions. For example, we can state (and prove) the correctness map
fusion for lists as a lemma:

        fuse : ∀xs : List A, f : A → B, g : B → C. 
               map g (map f xs) = map (g ∘ f) xs 

   Note that because we do not have a function extensionality axiom, we 
have to state fusion in a "pointwise" style. This is exactly the
same as in intensional type theories such as Coq and Agda (which also
lack function extensionality). 

	
