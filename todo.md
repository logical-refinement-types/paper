TODO
----






TODONE
------

+: changes made
o: no change necessary
-: change needed, but not done 

+ Line 1001: Which parts of the proof can be effectively automated?
+ Line 149: What is "a bracketing theorem"?

+ Line 97: Is "=" in "(x = y)" Boolean equality? How is that defined?

o Line 28: What are "the bookkeeping details"?

o l1157 p24: “there is a deeper problem with using squash types”: I'd say this problem is an inessential property of squash types. The elimination rule given by Awodey and Bauer is the one suggested by topos theory, which, as you pointedly remark, precludes erasure (this shouldn't be surprising, in a topos, a function is the same as a functional relation, so you can define all your computation in your type of proposition, Kraus demonstrates it very starkly though). But you could use a more restrictive elimination rule which would be compatible with erasure. But it wouldn't change the fact that if the bracket can be surjective onto propositions, it couldn't be surjective on proofs if you want to support classical logic.

+ l1144 p24: it's true that SAT solvers are in NP while intuitionistic propositional logic is PSPACE-complete. However, you can still use a SAT solver to solve intuitionistic propositions (it's going to be less efficient, though). See SAT Modulo Intuitionistic Implications.

o l953 p20: “this is also the reason […]”: my understanding is that the lack of functional extensionality in your system is a simplification, rather than an essential feature of the system. If so, I'd recommend saying so earlier, when you first speak of extensionality.
+ l773 p16: your explanation of why the context bindings are interpreted in M ⟦A⟧M [[A]] is quite welcome. Though you may want to add a little bit of explanation of why you will want to substitute by non-value expressions.

+ l670 p14: it is not clear why the only meaningful interaction between proofs of equality and terms is via ex falso. There could be a substitution function over terms. But there isn't maybe it's a good opportunity to explain why.

+ l282 p6: you're speaking of intersection and union types for ∀∀ and ∃∃. You use this terminology throughout the article. Why not, it's quite reasonable, but it's not particularly common. I would strongly recommend introducing the terminology to avoid suprising/confusing the reader.

+ l250 p6: this is the first occurrence of propositions, in argument positions, being erased to the unit type. You only explain why much later (<where>). Where you say that it is meant to “avoid issues with eager evaluation”. I think you should give a sentence or two of explanation here (on p6). One thing that I don't think is ever clarified is what kind of issues your encounter. My guess is that you don't evaluate under ghost λλ, so erasing the argument would change the semantics of the function. You could evaluate under ghost λλ, I assume, but maybe it's simpler not to?

o l374 p8, l757 p16: Make sure not to leave references to the appendix in the camera ready.
+ Rule Eq-WF: Can A be a function type? Comparing functions hasn't worked well in refinement types.
+ 1.1 there is no paper outline, maybe adding section references to the contributions can give an implicit paper outline.
+ line 34: space before citation
+ line 88-98: having said that your system has no judgmental equality, the (un)equalities here deserve some explanation
+ line 105: Even though VCs quickly becoming undecidable is easy to believe, the examples you provide in the paper all seem to be in the decidable SMT fragments.
+ line 136: typo "a terms"
+ line 159: maybe typo "unityped"
+ line 183: why the first lambda doesn't have a hat?
+ line 190: here it is len instead of List.len
+ line 200: len here should be Vec.len?
+ line 231: what is ";"?
o line 387: it does not seem safe to upgrade. when it is ok? 
+ line 407, 416: "." missing from the first bullet
+ line 705: missing closing parenthesis.
+ line 748: go wrong." -> don't go wrong".
+ Line 1185: What is MLTT?
+ Line 926: "represents elements A" -> "represents elements of A"
+ Line 749: The paper uses "error monad", "option monad", "exception monad", etc. for the same thing. Maybe just stick to one.
+ Line 305: "zero_comm" appears twice in Fig. 1.
+ Line 38: "SMT solver is in use" -> "SMT solver that is in use"
+ Line 163: "informed" -> "inspired" (did influenced)
+ Line 668: “dual to the term formers”: I don't think “dual” is the appropriate terminology here, as it implies some form of covariance. Maybe “which for the most part mirror the term formers”
+ Line 630: in the rule ηirηir​, last premise should be f ∥x∥=Bg ∥x∥f ∥x∥=B​g ∥x∥ shouldn't it?
+ Line 757: When you introduce the monad MM, or somewhere in this introduction, it is probably worth making explicit that you are working in (the category of) sets.
+ Line 654: it would seem that the link on Uniq points to the wrong Figure.
+ Line 973: it's probably being pendantic, but instead of “Termination”, I'd say “Convergence” (as it can be argued that a function which diverges with an error, it does still terminates).
+ Line 332: The base case of induction seems to require "0 + m = m + 0" but zero_comm gives "m + 0 = 0 + m".
+ Line 320: What is "congr"?
+ Line 321: What is "symm"?
+ Line 410: "strictly more" might not hold if there are no ghost variables.
+ Check all references to \ref{sec:discussion} and \ref{sec:related-work}. 



