# Explicit Refinement Types Lean 4 Formalization

This is a formalization of Explicit Refinement Types in Lean 4. 

The current version has been checked to compile with Lean version `leanprover/lean4:nightly-2023-02-24`, but Lean 4 is unstable software and later updates may lead to breakage.

To compile, simply run `lake build`